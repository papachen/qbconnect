﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace QBConnect
{
    [Serializable()]
    [XmlRoot("CustomerQueryRs")]
    public class CustomerRes
    {
        public static string root = "//CustomerQueryRs";
        [XmlAttribute("statusMessage")]
        public string status { get; set; }
        [XmlElement("CustomerRet")]
        public CustomerRet[] Customers { get; set; }
    }

    public class CustomerRet
    {
        [XmlElement("CustomerRet")]

        public string Name { get; set; }
        public string FullName { get; set; }
        public string CompanyName { get; set; }
        public bool IsActive { get; set; }
        [XmlElement("BillAddress")]
        public BillAddress billaddr { get; set; }
        [XmlElement("ShipAddress")]
        public ShipAddress shipaddr { get; set; }
    }

    public class ShipAddress
    {
        [XmlElement("Addr1")]
        public string Addr1 { get; set; }
        [XmlElement("Addr2")]
        public string Addr2 { get; set; }
    }

    public class BillAddress
    {
        [XmlElement("Addr1")]
        public string Addr1 { get; set; }
        [XmlElement("Addr2")]
        public string Addr2 { get; set; }
    }
 }
