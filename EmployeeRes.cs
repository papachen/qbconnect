﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace QBConnect
{
    [Serializable()]
    [XmlRoot("EmployeeQueryRs")]
    public class EmployeeRes
    {
        public static string root = "//EmployeeQueryRs";
        [XmlAttribute("statusMessage")]
        public string status;
        [XmlElement("EmployeeRet")]
        public Employees[] Employee;
    }

    public class Employees
    {
        [XmlElement("IsActive")]
        public bool IsActive;
        [XmlElement("Name")]
        public string Name;
        [XmlElement("FirstName")]
        public string FirstName;
        [XmlElement("LastName")]
        public string LastName;
        [XmlElement("Department")]
        public string Department;
        [XmlElement("JobTitle")]
        public string JobTitle;
        [XmlElement("Description")]
        public string Description;
        [XmlElement("SupervisorRef")]
        public Supervisor Supervisor;
    }

    public class Supervisor
    {
        [XmlElement("ListID")]
        public string ListID;
        [XmlElement("FullName")]
        public string FullName;
    }
}
