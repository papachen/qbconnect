﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using xl = Microsoft.Office.Interop.Excel;
using NLog;

namespace QBConnect
{
    public class DataProcess
    {
        protected string excelfile;
        protected xl.Application xlApp;
        protected object misValue;
        protected Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
        protected Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;

        public DataProcess(string path)
        {
            excelfile = path;
            xlApp = new xl.Application();
            misValue = System.Reflection.Missing.Value;
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = xlWorkBook.ActiveSheet;
            if (xlApp == null)
            {
                Program.Log("Excel is not properly installed!!", LogLevel.Error);
            }
        }

        public void CreateExcel()
        {

        }

        public void AddSheet(CustomerRes cus)
        {
            Microsoft.Office.Interop.Excel.Worksheet xSheet = xlWorkBook.Sheets.Add(misValue, xlWorkSheet, 1, misValue);
            xSheet.Name = "Customer";
            xlWorkSheet = xSheet;

            //  Writing the header
            for (var j = 1; j <= Program._conf.customerheader["CustomerQuery"].Count(); j++)
            {
                xSheet.Cells[1, j] = Program._conf.customerheader["CustomerQuery"][j - 1].header;
            }
            
            //  Writing the data
            var i = 2;
            foreach (CustomerRet c in cus.Customers)
            {
                if (c.IsActive)
                {
                    xSheet.Cells[i, 1] = c.Name;
                    xSheet.Cells[i, 2] = c.CompanyName;
                    xSheet.Cells[i, 3] = c.FullName;
                    if (c.billaddr != null)
                    {
                        xSheet.Cells[i, 4] = c.billaddr.Addr1 + " " + c.billaddr.Addr2;
                    }
                    i++;
                }
            }
        }

        public void AddSheet(EmployeeRes emp)
        {
            Microsoft.Office.Interop.Excel.Worksheet xSheet = xlWorkBook.Sheets.Add(misValue, xlWorkSheet, 1, misValue);
            xSheet.Name = "Employee";
            xlWorkSheet = xSheet;

            //  Writing the header
            for (var j = 1; j <= Program._conf.customerheader["EmployeeQuery"].Count(); j++)
            {
                xSheet.Cells[1, j] = Program._conf.customerheader["EmployeeQuery"][j - 1].header;
            }

            //  Writing the data
            var i = 2;
            foreach (Employees e in emp.Employee)
            {
                if (e.IsActive)
                {
                    xSheet.Cells[i, 1] = e.Name;
                    xSheet.Cells[i, 2] = e.FirstName;
                    xSheet.Cells[i, 3] = e.LastName;
                    xSheet.Cells[i, 4] = e.Department;
                    if (e.Supervisor != null)
                    {
                        xSheet.Cells[i, 5] = e.Supervisor.FullName;
                    }
                    xSheet.Cells[i, 6] = e.Description;
                    i++;
                }
            }
        }

        public void AddSheet(ItemServiceRes item)
        {
            Microsoft.Office.Interop.Excel.Worksheet xSheet = xlWorkBook.Sheets.Add(misValue, xlWorkSheet, 1, misValue);
            xSheet.Name = "Item Services";
            xlWorkSheet = xSheet;

            //  Writing the header
            for (var j = 1; j <= Program._conf.customerheader["ItemServiceQuery"].Count(); j++)
            {
                xSheet.Cells[1, j] = Program._conf.customerheader["ItemServiceQuery"][j - 1].header;
            }

            //  Writing the data
            var i = 2;
            foreach (Items ii in item.Item)
            {
                if (ii.IsActive)
                {
                    xSheet.Cells[i, 1] = ii.Name;
                    xSheet.Cells[i, 2] = ii.Fullname;
                    i++;
                }
            }
        }

        public void AddSheet(PayrollItemWageRes pay)
        {
            Microsoft.Office.Interop.Excel.Worksheet xSheet = xlWorkBook.Sheets.Add(misValue, xlWorkSheet, 1, misValue);
            xSheet.Name = "PayrollItem";
            xlWorkSheet = xSheet;

            //  Writing the header
            for (var j = 1; j <= Program._conf.customerheader["PayrollItemWageQuery"].Count(); j++)
            {
                xSheet.Cells[1, j] = Program._conf.customerheader["PayrollItemWageQuery"][j - 1].header;
            }

            //  Writing the data

            var i = 2;
            foreach (PayrollItem p in pay.Item)
            {
                if (p.IsActive)
                {
                    xSheet.Cells[i, 1] = p.Name;
                    xSheet.Cells[i, 2] = p.Type;
                    i++;
                }
            }
        }

        public void Save()
        {
            try
            {
                xlApp.DisplayAlerts = false;
                xlWorkSheet = xlWorkBook.Worksheets["Sheet1"];
                if(xlWorkSheet != null)  xlWorkSheet.Delete();
                xlWorkBook.SaveAs(excelfile, Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(0);
                xlApp.Quit();
                Program.Log("Closing Excel", LogLevel.Info);
                Program.Log("Form Data file created!!", LogLevel.Info);
            }
            catch (Exception ex)
            {
                Program.Log("Excel File Creation Error: " + ex.Message, LogLevel.Fatal);
            }

            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                try
                {
                    Marshal.ReleaseComObject(xlWorkSheet);
                    Marshal.ReleaseComObject(xlWorkBook);
                    Marshal.ReleaseComObject(xlApp);
                }
                catch(Exception ex)
                {
                    Program.Log(ex.Message, LogLevel.Error);
                }

            }

        }

        public List<XmlDocument> TimeTrackingAdd(string f, string x)
        {
            List<XmlDocument> xmls = new List<XmlDocument>();
            string[] header = null;
            var line = 1;
            using (var reader = new StreamReader(f))
            {
                while (!reader.EndOfStream)
                {
                    if (line <= 1)
                    {
                        header = reader.ReadLine().Split(',');
                        line++;
                        continue;
                    }
                    XmlDocument tempX = new XmlDocument();
                    tempX.LoadXml(x);
                    XmlNode TimeTrackingAdd = tempX.SelectSingleNode("//TimeTrackingAdd");
                    string[] timeEntry = (reader.ReadLine().Split(','));   // The order of each invoice item will follow the "InvoiceAdd" item in QBConnect.config.xml

                    for (var i = 0; i < timeEntry.Length; i++)
                    {
                        switch (header[i])
                        {
                            case "TxnDate":
                                TimeTrackingAdd.SelectSingleNode("//TxnDate").InnerText = getDate(timeEntry[i]);
                                break;
                            case "EntityRef":
                                TimeTrackingAdd.SelectSingleNode("//EntityRef/FullName").InnerText = timeEntry[i];
                                break;
                            case "CustomerRef":
                                TimeTrackingAdd.SelectSingleNode("//CustomerRef/FullName").InnerText = timeEntry[i];
                                break;
                            case "ItemRef":
                                TimeTrackingAdd.SelectSingleNode("//ItemServiceRef/FullName").InnerText = timeEntry[i];
                                break;
                            case "Duration":
                                TimeTrackingAdd.SelectSingleNode("//Duration").InnerText = getDuration(timeEntry[i]);
                                break;
                            case "ClassRef":
                                TimeTrackingAdd.SelectSingleNode("//ClassRef/FullName").InnerText = timeEntry[i];
                                break;
                            case "PayrollItemWageRef":
                                TimeTrackingAdd.SelectSingleNode("//PayrollItemWageRef/FullName").InnerText = timeEntry[i];
                                break;
                            case "Notes":
                                TimeTrackingAdd.SelectSingleNode("//Notes").InnerText = timeEntry[i];
                                break;
                        }
                    }

                    try
                    {
                        xmls.Add(tempX);
                        TimeTrackingAdd = null;
                        tempX = null;
                    }
                    catch (Exception ex)
                    {
                        Program.Log("Creating QBXML Error: " + ex.Message, LogLevel.Error);
                    }
                }
            }
            return xmls;
        }

    public List<XmlDocument> InvoiceAddRequest(string f, string x)
        {
            List<XmlDocument> xmls = new List<XmlDocument>();
            string[] header = null;
            var line = 1;
            using (var reader = new StreamReader(f))
            {
                while (!reader.EndOfStream)
                {
                    if(line <=1)
                    {
                        header = reader.ReadLine().Split(',');
                        line++;
                        continue;
                    }
                    XmlDocument tempX = new XmlDocument();
                    tempX.LoadXml(x);
                    XmlDocument a = new XmlDocument();
                    a.LoadXml(x);
                    XmlNode InvoiceAdd = tempX.SelectSingleNode("//InvoiceAdd");
                    XmlNode InvoiceLineAdd = tempX.SelectSingleNode("//InvoiceLineAdd");
                    string[] invoices = (reader.ReadLine().Split(','));   // The order of each invoice item will follow the "InvoiceAdd" item in QBConnect.config.xml

                    for (var i = 0; i < invoices.Length; i++)
                    {
                        switch (header[i])
                        {
                            case "CustomerRef":
                                InvoiceAdd.SelectSingleNode("//CustomerRef/FullName").InnerText = invoices[i];
                                break;
                            case "TxnDate":
                                InvoiceAdd.SelectSingleNode("//TxnDate").InnerText = invoices[i];
                                break;
                            case "RefNumber":
                                InvoiceAdd.SelectSingleNode("//RefNumber").InnerText = invoices[i];
                                break;
                            case "Memo":
                                InvoiceAdd.SelectSingleNode("//Memo").InnerText = invoices[i];
                                break;
                            case "TermsRef":
                                InvoiceAdd.SelectSingleNode("//TermsRef/FullName").InnerText = invoices[i];
                                break;
                            case "ItemRef":
                                InvoiceLineAdd.SelectSingleNode("//ItemRef/FullName").InnerText = invoices[i];
                                break;
                            case "Desc":
                                InvoiceLineAdd.SelectSingleNode("//Desc").InnerText = invoices[i];
                                break;
                            case "Quantity":
                                InvoiceLineAdd.SelectSingleNode("//Quantity").InnerText = invoices[i];
                                break;
                            case "Rate":
                                InvoiceLineAdd.SelectSingleNode("//Rate").InnerText = decimal.Parse(invoices[i]).ToString("#.##");
                                break;
                            case "Amount":
                                InvoiceLineAdd.SelectSingleNode("//Amount").InnerText = Convert.ToDecimal(invoices[i], CultureInfo.InvariantCulture).ToString("F2");
                                break;
                        }
                    }

                    try
                    {
                        xmls.Add(tempX);
                        InvoiceAdd = null;
                        tempX = null;
                        InvoiceLineAdd = null;
                    }
                    catch(Exception ex)
                    {
                        Program.Log("Creating QBXML Error: " + ex.Message, LogLevel.Error);
                    }
                }
            }
            return xmls;
        }

        public string getDate(string date)
        {
            var newdate = date.Contains("/") ? date.Split('/') : date.Split('-');
            DateTime dd = new DateTime(Int32.Parse(newdate[0]), Int32.Parse(newdate[1]), Int32.Parse(newdate[2]));
            return dd.ToString("yyyy-MM-dd");
        }

        public string getDuration(string time)
        {
            var timestring = time.Split('.');
            int min = Int32.Parse(timestring[1]) * 60 / 100;
            return "PT" + timestring[0] + "H" + min + "M";
        }
    }
}
