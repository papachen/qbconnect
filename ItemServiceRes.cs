﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace QBConnect
{
    [Serializable()]
    [XmlRoot("ItemServiceQueryRs")]
    public class ItemServiceRes
    {
        public static string root = "//ItemServiceQueryRs";
        [XmlAttribute("statusMessage")]
        public string status { get; set; }
        [XmlElement("ItemServiceRet")]
        public Items[] Item { get; set; }
    }

    public class Items
    {
        [XmlElement("IsActive")]
        public bool IsActive { get; set; }
        [XmlElement("Name")]
        public string Name { get; set; }
        [XmlElement("FullName")]
        public string Fullname { get; set; }
    }
}
