﻿using System;
using System.Windows.Forms;
using System.ServiceProcess;
using System.Diagnostics;
using System.Linq;
using Interop.QBXMLRP2Lib;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using System.Dynamic;
using NLog;

namespace QBConnect
{
    /// <summary>
    /// 
    /// </summary>

     static class Program
	{
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        // Initialization

        static Timer t = new Timer();
        static string qbwpath;
        static string appID;
        static string csv;
        static string[] qbexport;
        static string[] qbimport;
        static public Config _conf;
        static public DataProcess mydata;
        static public Query myQuery;

        [STAThread]
		static void Main()
		{
            try
            {
                _conf = new Config();   // Reads configuration

                int _timer;

                if (_conf == null)
                {
                    MessageBox.Show("No Configuration Found!!", "Configuration Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }

                qbwpath = _conf.qbwpath;
                appID = _conf.appid;
                _timer = _conf.timer;

                qbexport = _conf.exportlist;
                qbimport = _conf.importlist;

                if (qbwpath == "")
                {
                    RequestProcessor2 rp0 = new RequestProcessor2();
                    rp0.OpenConnection("AppID", appID);
                    var ticket = "";
                    try
                    {
                        ticket = rp0.BeginSession("", QBFileMode.qbFileOpenDoNotCare);
                        XmlDocument x = new XmlDocument();
                        x.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\"?><?qbxml version=\"13.0\"?><QBXML><QBXMLMsgsRq onError=\"stopOnError\"><CheckQueryRq requestID=\"1\"/></QBXMLMsgsRq></QBXML>");
                        var output = rp0.ProcessRequest(ticket, x.OuterXml);

                        MessageBox.Show("Your QBConnect is now registered with QuickBooks Desktop!!", "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Log(ex.Message, LogLevel.Error);
                    }
                    finally
                    {
                        rp0.EndSession(ticket);
                        rp0.CloseConnection();
                        Application.Exit();
                    }
                }
                else if (!File.Exists(qbwpath))
                {
                    MessageBox.Show("Please make sure the QuickBooks Company File is provided to start the application!!", "Configuration Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }

                if(_timer == 0)
                {
                    MessageBox.Show("Please set the pulling timer to greater than 0 minute to start the application!!", "Configuration Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }

                if (qbexport.Length == 0 && qbimport.Length == 0)
                {
                    MessageBox.Show("No QuickBooks features are enabled!!", "Configuration Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                myQuery = new Query();
                InitializeTimer(_timer);

                // Show the system tray icon.					
                using (ProcessIcon pi = new ProcessIcon())
                {
                    pi.Display();

                    // Make sure the application runs!
                    Application.Run();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Configuration Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }

        private static void InitializeTimer(int time)
        {
            t.Tick += new EventHandler(Timer1_Tick);
            t.Interval = time;
            t.Enabled = true;
            QBIntegrate();
        }

        private static void QBIntegrate()
        {
            FileInfo f = new FileInfo(qbwpath);
            if(IsFileLocked(f))
            {
                Log("Company File is currently been used, skip for next run...", LogLevel.Trace);
                return;
            }

            Log("Process Started...", LogLevel.Info);

            RequestProcessor2 rp = new RequestProcessor2();
            mydata = new DataProcess(_conf.xlsx);

            string ticket = null;
            string output = null;

            QBFileMode qbfilemode;

            if (_conf.qbfilemode == "2")
            {
                qbfilemode = QBFileMode.qbFileOpenDoNotCare;
            }
            else if(_conf.qbfilemode == "1")
            {
                qbfilemode = QBFileMode.qbFileOpenMultiUser;
            }
            else
            {
                qbfilemode = QBFileMode.qbFileOpenSingleUser;
            }

            try
            {
                rp.OpenConnection("AppID", appID);
                ticket = rp.BeginSession(@qbwpath, qbfilemode);

                int n;
                //  Get Export feature list
                if (qbexport != null)
                {
                    var exportList = myQuery.getFeatures(qbexport);
                    Serializer ser = new Serializer();
                    n = 0;
                    foreach (string input in exportList)
                    {
                        output = null;
                        output = rp.ProcessRequest(ticket, input);
                        XmlDocument x = new XmlDocument();
                        x.LoadXml(output);
                        switch (qbexport[n])
                        {
                            case "CustomerQuery":
                                CustomerRes customers = (CustomerRes)ser.Deserialize<CustomerRes>(x.SelectSingleNode(CustomerRes.root));
                                if (customers.Customers.Length == 0)
                                {
                                    Log("No Customer Records Found!!", LogLevel.Info);
                                }
                                else
                                {
                                    mydata.AddSheet(customers);
                                }
                                break;
                            case "EmployeeQuery":
                                EmployeeRes emp = (EmployeeRes)ser.Deserialize<EmployeeRes>(x.SelectSingleNode(EmployeeRes.root));
                                if (emp.Employee.Length == 0)
                                {
                                    Log("No Employee Records Found!!", LogLevel.Info);
                                }
                                else
                                {
                                    mydata.AddSheet(emp);
                                }
                                break;
                            case "ItemServiceQuery":
                                ItemServiceRes items = (ItemServiceRes)ser.Deserialize<ItemServiceRes>(x.SelectSingleNode(ItemServiceRes.root));
                                if (items.Item.Length == 0)
                                {
                                    Log("No Item Records Found!!", LogLevel.Info);
                                }
                                else
                                {
                                    mydata.AddSheet(items);
                                }
                                break;
                            case "PayrollItemWageQuery":
                                PayrollItemWageRes pay = (PayrollItemWageRes)ser.Deserialize<PayrollItemWageRes>(x.SelectSingleNode(PayrollItemWageRes.root));
                                if (pay.Item.Length == 0)
                                {
                                    Log("No PayrollWage Records Found!!", LogLevel.Info);
                                }
                                else
                                {
                                    mydata.AddSheet(pay);
                                }
                                break;
                        }
                        n++;
                    }

                    //  Writing QB Data to Excel file
                    mydata.Save();
                }

                if(qbimport != null)
                { 
                    var importList = myQuery.getFeatures(qbimport);
                    for (var i = 0; i < importList.Count; i++)
                    {
                        output = null;
                        csv = _conf.cvsinput[i];

                        if (File.Exists(csv))
                        {
                            List<XmlDocument> xmlInput = new List<XmlDocument>();
                            switch (qbimport[i])
                            {
                                case "InvoiceAdd":
                                    xmlInput = mydata.InvoiceAddRequest(csv, importList[i]);
                                    break;
                                case "TimeTrackingAdd":
                                    xmlInput = mydata.TimeTrackingAdd(csv, importList[i]);
                                    break;
                            }

                            foreach (XmlDocument xml in xmlInput)
                            {
                                var input = xml.OuterXml;
                                output = rp.ProcessRequest(ticket, input);
                                if (QBXMLResponse(output, qbimport[i]))
                                {
                                    File.Delete(csv);
                                    Log("Import Item Successfull!! Delete CSV file!", LogLevel.Info);
                                }
                                else
                                {
                                    Log("Import Item Error: " + output, LogLevel.Error);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ex.Message, LogLevel.Fatal);
            }
            finally
            {
                try
                {
                    rp.EndSession(ticket);
                    rp.CloseConnection();
                    Log("Close QuickBooks Connection...", LogLevel.Info);
                }
                catch(Exception ex)
                {
                    Log(ex.Message, LogLevel.Error);
                }
            }
        }

        private static void Timer1_Tick(object Sender, EventArgs e)
            {
                QBIntegrate();
            }

        public static Boolean QBXMLResponse(string xml, string q)
        {
            XmlDocument x = new XmlDocument();
            x.LoadXml(xml);
            XmlNode i;
            switch (q)
            {
                case "ItemServiceAdd":
                    i = x.SelectSingleNode("//ItemServiceAddRs");
                    break;
                case "TimeTrackingAdd":
                    i = x.SelectSingleNode("//TimeTrackingAddRs");
                    break;
                case "InvoiceAdd":
                    i = x.SelectSingleNode("//InvoiceAddRs");
                    break;
                default:
                    return false;
            }
            if (i.Attributes["statusSeverity"].Value == "Error")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        static private bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static void Log(string logMessage, LogLevel type)
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Log(type, logMessage);
        }
    }
}