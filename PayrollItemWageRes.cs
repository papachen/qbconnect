﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace QBConnect
{
    [Serializable()]
    [XmlRoot("PayrollItemWageQueryRs")]
    public class PayrollItemWageRes
    {
        public static string root = "//PayrollItemWageQueryRs";
        [XmlAttribute("statusMessage")]
        public string status;
        [XmlElement("PayrollItemWageRet")]
        public PayrollItem[] Item;
    }

    public class PayrollItem
    {
        [XmlElement("IsActive")]
        public bool IsActive;
        [XmlElement("Name")]
        public string Name;
        [XmlElement("WageType")]
        public string Type;
    }
}
