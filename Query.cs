﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using NLog;

namespace QBConnect
{
    public class Query
    {
        protected string folder = "";

        public Query()
        {
            folder = @"QBXML\";
        }

        public List<string> getFeatures(string[] list)
        {
            List<string> features = new List<string>();
            foreach (string f in list)
            {
                XmlDocument xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.Load(@folder + f + ".xml");
                    features.Add(xmlDoc.OuterXml);
                    Program.Log(string.Format("Loading feature: {0}", f), LogLevel.Info);
                }
                catch (Exception ex)
                {
                    Program.Log(string.Format("Loading feature failed: {0}", ex.Message), LogLevel.Error);
                }
            }
            return features;
        }
    }
}
