﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace QBConnect
{
    class Serializer
    {
        public T Deserialize<T>(XmlNode n) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(n.OuterXml))
            {
                return (T)ser.Deserialize(sr);
            }
        }
    }
}
