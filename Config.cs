﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;

namespace QBConnect
{
    public class Config
    {
        public string appid;
        public int timer;
        public string qbwpath;
        public string qbfilemode;
        public string dir;
        public string[] exportlist;
        public string[] importlist;
        public Dictionary<string, List<directory>> customerheader;
        public List<string> cvsinput;
        public string xlsx;

        public Config()
        {
            this.appid = ConfigurationManager.AppSettings["AppID"];
            this.timer = Int32.Parse(ConfigurationManager.AppSettings["SyncPollFrequencyMinutes"]) * 1000 * 60;
            this.qbwpath = ConfigurationManager.AppSettings["QBWPath"];
            this.qbfilemode = ConfigurationManager.AppSettings["QBFileMode"];
            this.dir = ConfigurationManager.AppSettings["BasedDir"];
            this.exportlist = ConfigurationManager.AppSettings["Export"].Split(',');
            this.importlist = ConfigurationManager.AppSettings["Import"].Split(',');
            this.customerheader = new Dictionary<string, List<directory>>();
            this.cvsinput = new List<string>();
            this.xlsx = ConfigurationManager.AppSettings["Xlsx"];

            foreach (string elist in exportlist)
            {
                customerheader.Add(elist, ConfigurationManager.GetSection(elist) as List<directory>);
            }

            var temp = ConfigurationManager.GetSection("importList") as List<directory>;
            for (var i = 0; i < temp.Count; i++)
            {
                cvsinput.Add(temp[i].header);
            }
        }
    }

    public class CustomerHeader : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            List<directory> myConfigObject = new List<directory>();

            foreach (XmlNode childNode in section.ChildNodes)
            {
                foreach (XmlAttribute attrib in childNode.Attributes)
                {
                    myConfigObject.Add(new directory() { header = attrib.Value });
                }
            }
            return myConfigObject;
        }
    }

    public class EmployeeHeader : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            List<directory> myConfigObject = new List<directory>();

            foreach (XmlNode childNode in section.ChildNodes)
            {
                foreach (XmlAttribute attrib in childNode.Attributes)
                {
                    myConfigObject.Add(new directory() { header = attrib.Value });
                }
            }
            return myConfigObject;
        }
    }

    public class ItemHeader : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            List<directory> myConfigObject = new List<directory>();

            foreach (XmlNode childNode in section.ChildNodes)
            {
                foreach (XmlAttribute attrib in childNode.Attributes)
                {
                    myConfigObject.Add(new directory() { header = attrib.Value });
                }
            }
            return myConfigObject;
        }
    }

    public class PayrollItemWageHeader : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            List<directory> myConfigObject = new List<directory>();

            foreach (XmlNode childNode in section.ChildNodes)
            {
                foreach (XmlAttribute attrib in childNode.Attributes)
                {
                    myConfigObject.Add(new directory() { header = attrib.Value });
                }
            }
            return myConfigObject;
        }
    }

    public class directory
    {
        public string header { get; set; }
    }

    public class ImportList : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            List<directory> myConfigObject = new List<directory>();

            foreach (XmlNode childNode in section.ChildNodes)
            {
                foreach (XmlAttribute attrib in childNode.Attributes)
                {
                    myConfigObject.Add(new directory() { header = attrib.Value });
                }
            }
            return myConfigObject;
        }
    }
}
